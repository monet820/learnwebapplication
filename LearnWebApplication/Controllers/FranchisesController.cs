﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnWebApplication.Models;
using LearnWebApplication.Dtos;
using AutoMapper;

namespace LearnWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all the franchises.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            var FranchiseList = await _context.Franchises.ToListAsync();

            List<FranchiseDto> Franchises = new List<FranchiseDto>();

            foreach (var item in FranchiseList)
            {
                Franchises.Add(_mapper.Map<FranchiseDto>(item));
            }

            return Ok(Franchises);
        }

        /// <summary>
        /// Returns all the movies in the selected franchise.
        /// </summary>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<FranchiseMoviesDto>>> GetFranchiseMovies(int id)
        {
            var movies = await _context.Franchises.Where(a => a.Id == id).SelectMany(a => a.Movie).ToListAsync();

            List<FranchiseMoviesDto> FranchiseMovies = new List<FranchiseMoviesDto>();

            foreach (var item in movies)
            {
                FranchiseMovies.Add(_mapper.Map<FranchiseMoviesDto>(item));
            }

            return Ok(FranchiseMovies);
        }


        /// <summary>
        /// Returns all the characters who played in the franchise, selected by id.
        /// </summary>
        [HttpGet("{id}/Characters")]
        public async Task<IActionResult> GetFranchiseCharacters(int id)
        {
            var franchise = await _context.Franchises.Where(a => a.Id == id).SelectMany(a => a.Movie).SelectMany(m => m.MovieCharacter).Select(a => a.Character).ToListAsync();

            return Ok(franchise);
        }

        /// <summary>
        /// Returns the selected franchise, by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FirstOrDefaultAsync(a => a.Id == id);
            return Ok(franchise);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Deletes the selected franchise, by id.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
