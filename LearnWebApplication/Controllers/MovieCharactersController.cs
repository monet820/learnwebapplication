﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnWebApplication.Models;
using LearnWebApplication.Dtos;
using AutoMapper;

namespace LearnWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly MovieContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(MovieContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Returns all the moviecharacters.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetAll(int id)
        {
            var MovieList = await _context.MovieCharacters.Include(a => a.Actor).Include(b => b.Character).Include(c => c.Movie).ToListAsync();

            List<MovieCharacterDto> Movies = new List<MovieCharacterDto>();

            foreach (var item in MovieList)
            {
                Movies.Add(_mapper.Map<MovieCharacterDto>(item));
            }

            return Ok(Movies);
        }

        // PUT: api/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieCharacter(int id, MovieCharacter movieCharacter)
        {
            if (id != movieCharacter.MovieID)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.MovieCharacters.Add(movieCharacter);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(movieCharacter.MovieID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new { id = movieCharacter.MovieID }, movieCharacter);
        }

        // DELETE: api/MovieCharacters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacters.FindAsync(id);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }

        private bool MovieCharacterExists(int id)
        {
            return _context.MovieCharacters.Any(e => e.MovieID == id);
        }
    }
}
