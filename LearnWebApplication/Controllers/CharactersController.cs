﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnWebApplication.Models;
using LearnWebApplication.Dtos;
using AutoMapper;

namespace LearnWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all the characters.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacters()
        {
            var characterList = await _context.Characters.ToListAsync();

            List<CharacterDto> Characters = new List<CharacterDto>();

            foreach (var item in characterList)
            {
                Characters.Add(_mapper.Map<CharacterDto>(item));
            }

            return Ok(Characters);
        }

        /// <summary>
        /// Returns all the actors who have played the selected character by id.
        /// </summary>
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<IEnumerable<CharacterActorsDto>>> GetCharacterActors(int id)
        {
            var actors = await _context.Movies.SelectMany(m => m.MovieCharacter).Where(mc => mc.CharacterID == id).Select(ma => ma.Actor).ToListAsync();

            List<CharacterActorsDto> actorMovies = new List<CharacterActorsDto>();

            foreach (var item in actors)
            {
                actorMovies.Add(_mapper.Map<CharacterActorsDto>(item));
            }

            return Ok(actorMovies);
        }

        /// <summary>
        /// Return the selected character, by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Character>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return character;
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        /// <summary>
        /// Deletes the selected character, by id.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
