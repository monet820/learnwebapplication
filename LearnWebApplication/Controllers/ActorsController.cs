﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnWebApplication.Models;
using AutoMapper;
using LearnWebApplication.Dtos;

namespace LearnWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : Controller
    {
        private readonly MovieContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MovieContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Returns list of actors.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActors()
        {
            var actorslist = await _context.Actors.ToListAsync();

            List<ActorDto> actors = new List<ActorDto>();

            foreach (var item in actorslist)
            {
                actors.Add(_mapper.Map<ActorDto>(item));
            }

            return Ok(actors);
        }

        /// <summary>
        /// Returns the selected actor by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetActor(int id)
        {
            Actor actor = await _context.Actors.FirstOrDefaultAsync(a => a.Id == id);

            var usermapped = _mapper.Map<ActorDto>(actor);

            return Ok(usermapped);
        }

        /// <summary>
        /// Returns all movies an actor has played in.
        /// </summary>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<ActorMoviesDto>>> GetActorMovies(int id)
        {
            var movies = await _context.Actors.Where(a => a.Id == id).SelectMany(a => a.MovieCharacter.Select(m => m.Movie)).ToListAsync();

            List<ActorMoviesDto> actorMovies = new List<ActorMoviesDto>();

            foreach (var item in movies)
            {
                actorMovies.Add(_mapper.Map<ActorMoviesDto>(item));
            }
            
            return Ok(actorMovies);
        }

        /// <summary>
        /// Returns all characters an actor has played.
        /// </summary>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<ActorCharactersDto>>> GetActorCharacter(int id)
        {
            var Characters = await _context.Actors.Where(a => a.Id == id).SelectMany(a => a.MovieCharacter.Select(mc => mc.Character)).ToListAsync();

            if (Characters == null)
            {
                return NotFound();
            }

            List<ActorCharactersDto> actorList = new List<ActorCharactersDto>();

            foreach (var item in Characters)
            {
                actorList.Add(_mapper.Map<ActorCharactersDto>(item));
            }

            return Ok(actorList);
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        /// <summary>
        /// Deletes the selected actor by id.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
