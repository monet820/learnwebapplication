﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LearnWebApplication.Models;
using AutoMapper;
using LearnWebApplication.Dtos;

namespace LearnWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieContext _context;
        public readonly IMapper _mapper;

        public MoviesController(MovieContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        /// <summary>
        /// Returns all the movies.
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
        {
            var MovieList = await _context.Movies.ToListAsync();

            List<MovieDto> Movies = new List<MovieDto>();

            foreach (var item in MovieList)
            {
                Movies.Add(_mapper.Map<MovieDto>(item));
            }

            return Ok(Movies);
        }

        /// <summary>
        /// Returns the selected movie, by id.
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        /// <summary>
        /// Returns all the characters and the actors from the selected movie, by id.
        /// </summary>
        [HttpGet("{id}/Characters/Actors")]
        public async Task<ActionResult<IEnumerable<MovieCharactersActorsDto>>> GetCharactersAndActors(int id)
        {
            var MovieList = await _context.MovieCharacters.Where(a => a.MovieID == id).Include(a => a.Actor).Include(b => b.Character).Include(c => c.Movie).ToListAsync();

            List<MovieCharactersActorsDto> Movies = new List<MovieCharactersActorsDto>();

            foreach (var item in MovieList)
            {
                Movies.Add(_mapper.Map<MovieCharactersActorsDto>(item));
            }

            return Ok(Movies);
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Deletes the selected movie, by id.
        /// </summary>       
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
