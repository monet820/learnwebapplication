﻿namespace LearnWebApplication.Dtos
{
    public class CharacterActorsDto
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}
