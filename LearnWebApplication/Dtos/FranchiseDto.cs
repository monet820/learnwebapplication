﻿namespace LearnWebApplication.Dtos
{
    public class FranchiseDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
