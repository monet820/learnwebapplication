﻿namespace LearnWebApplication.Dtos
{
    public class FranchiseMoviesDto
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseDate { get; set; }
        public string Director { get; set; }
    }
}
