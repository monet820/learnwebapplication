﻿namespace LearnWebApplication.Dtos
{
    public class MovieDto
    {
        public string Title { get; set; }
        public string Genre { get; set; }
    }
}
