﻿namespace LearnWebApplication.Dtos
{
    public class CharacterDto
    {
        public string FullName { get; set; }
        public string NameAlias { get; set; }
        public string Gender { get; set; }
    }
}
