﻿namespace LearnWebApplication.Dtos
{
    public class MovieCharacterDto
    {
        public MovieDto Movie { get; set; }
        public CharacterDto Character { get; set; }
        public ActorDto Actor { get; set; }
    }
}
