﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearnWebApplication.Dtos
{
    public class ActorMoviesDto
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseDate { get; set; }
        public string Director { get; set; }
    }
}
