﻿namespace LearnWebApplication.Dtos
{
    public class MovieCharactersActorsDto
    {
        public MovieDto Movie { get; set; }
        public CharacterDto Character { get; set; }
        public ActorDto Actor { get; set; }
    }
}
