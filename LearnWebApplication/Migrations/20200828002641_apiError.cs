﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class apiError : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[] { 1, 1, 3, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[] { 1, 3, 3, null });
        }
    }
}
