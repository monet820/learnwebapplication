﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class addingEntriesNext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[] { 1, 1, 1, null });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[] { 3, 2, 2, null });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[] { 2, 3, 3, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 3, 2 });
        }
    }
}
