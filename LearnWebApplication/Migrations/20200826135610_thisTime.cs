﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class thisTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<int>(nullable: false),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    ImageURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    NameAlias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    ImageURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FranchiseID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseDate = table.Column<int>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    ImageURL = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseID",
                        column: x => x.FranchiseID,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieID = table.Column<int>(nullable: false),
                    CharacterID = table.Column<int>(nullable: false),
                    ActorID = table.Column<int>(nullable: false),
                    ImageURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieID, x.CharacterID });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Actors_ActorID",
                        column: x => x.ActorID,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Characters_CharacterID",
                        column: x => x.CharacterID,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movies_MovieID",
                        column: x => x.MovieID,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DateOfBirth", "FirstName", "Gender", "ImageURL", "LastName", "MiddleName", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 1, null, 1977, "Jessica", "Female", null, "Chastain", null, "Sacramento" },
                    { 2, null, 1967, "Julia", "Female", null, "Roberts", null, "Smyrna" },
                    { 3, null, 1961, "George", "Male", null, "Clooney", null, "Lexington" }
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Gender", "ImageURL", "NameAlias" },
                values: new object[] { 1, "Detective James Falconer", "Male", "https://m.media-amazon.com/images/M/MV5BMTgxNzU2MDExNl5BMl5BanBnXkFtZTgwODYwMzUxMjI@._V1_SY1000_CR0,0,788,1000_AL_.jpg", "Falconer" });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 1, null, "Sisters" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseID", "Genre", "ImageURL", "ReleaseDate", "Title", "Trailer" },
                values: new object[] { 1, "James Atwater Contner", 1, "Drama", null, 1994, "I Only Have Eyes for You", null });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_ActorID",
                table: "MovieCharacter",
                column: "ActorID");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterID",
                table: "MovieCharacter",
                column: "CharacterID");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseID",
                table: "Movies",
                column: "FranchiseID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
