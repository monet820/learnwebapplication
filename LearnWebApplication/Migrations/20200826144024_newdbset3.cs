﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class newdbset3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Actors_ActorID",
                table: "MovieCharacter");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Characters_CharacterID",
                table: "MovieCharacter");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacter_Movies_MovieID",
                table: "MovieCharacter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter");

            migrationBuilder.RenameTable(
                name: "MovieCharacter",
                newName: "MovieCharacters");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacter_CharacterID",
                table: "MovieCharacters",
                newName: "IX_MovieCharacters_CharacterID");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacter_ActorID",
                table: "MovieCharacters",
                newName: "IX_MovieCharacters_ActorID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters",
                columns: new[] { "MovieID", "CharacterID" });

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Actors_ActorID",
                table: "MovieCharacters",
                column: "ActorID",
                principalTable: "Actors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Characters_CharacterID",
                table: "MovieCharacters",
                column: "CharacterID",
                principalTable: "Characters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacters_Movies_MovieID",
                table: "MovieCharacters",
                column: "MovieID",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Actors_ActorID",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Characters_CharacterID",
                table: "MovieCharacters");

            migrationBuilder.DropForeignKey(
                name: "FK_MovieCharacters_Movies_MovieID",
                table: "MovieCharacters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MovieCharacters",
                table: "MovieCharacters");

            migrationBuilder.RenameTable(
                name: "MovieCharacters",
                newName: "MovieCharacter");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacters_CharacterID",
                table: "MovieCharacter",
                newName: "IX_MovieCharacter_CharacterID");

            migrationBuilder.RenameIndex(
                name: "IX_MovieCharacters_ActorID",
                table: "MovieCharacter",
                newName: "IX_MovieCharacter_ActorID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MovieCharacter",
                table: "MovieCharacter",
                columns: new[] { "MovieID", "CharacterID" });

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Actors_ActorID",
                table: "MovieCharacter",
                column: "ActorID",
                principalTable: "Actors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Characters_CharacterID",
                table: "MovieCharacter",
                column: "CharacterID",
                principalTable: "Characters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MovieCharacter_Movies_MovieID",
                table: "MovieCharacter",
                column: "MovieID",
                principalTable: "Movies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
