﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class addingSeedUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Gender", "ImageURL", "NameAlias" },
                values: new object[] { 4, "Movie Test Character", "Male", "", "Baloneer" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
