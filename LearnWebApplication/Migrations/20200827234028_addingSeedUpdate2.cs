﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class addingSeedUpdate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DateOfBirth", "FirstName", "Gender", "ImageURL", "LastName", "MiddleName", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 4, null, 1996, "Sophie", "Female", null, "Turner", null, "Northampton" },
                    { 5, null, 1990, "Jennifer", "Female", null, "Lawrence", null, "Kentucky" },
                    { 6, null, 1989, "Nicholas", "Male", null, "Hoult", null, "Wokingham" }
                });

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1,
                column: "ImageURL",
                value: null);

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2,
                column: "ImageURL",
                value: null);

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: null);

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "FullName", "ImageURL", "NameAlias" },
                values: new object[] { "Hank McCoy", null, "Beast" });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Gender", "ImageURL", "NameAlias" },
                values: new object[,]
                {
                    { 5, "Raven", "Female", null, "Mystique" },
                    { 6, "Jean Grey", "Female", null, "Phoenix" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseID", "Genre", "ImageURL", "ReleaseDate", "Title", "Trailer" },
                values: new object[,]
                {
                    { 4, "Robert mandel", 1, "Drama", null, 1994, "Blinders", null },
                    { 5, "Kevin inch", 1, "Drama", null, 1994, "Bombshell", null },
                    { 6, "Kevin inch", 1, "Drama", null, 1994, "Up in the air", null }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieID", "CharacterID", "ActorID", "ImageURL" },
                values: new object[,]
                {
                    { 2, 4, 6, null },
                    { 2, 5, 5, null },
                    { 2, 6, 4, null },
                    { 4, 1, 3, null },
                    { 5, 1, 3, null },
                    { 6, 1, 3, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 2, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 2, 6 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 5, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieID", "CharacterID" },
                keyValues: new object[] { 6, 1 });

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1,
                column: "ImageURL",
                value: "https://m.media-amazon.com/images/M/MV5BMTgxNzU2MDExNl5BMl5BanBnXkFtZTgwODYwMzUxMjI@._V1_SY1000_CR0,0,788,1000_AL_.jpg");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2,
                column: "ImageURL",
                value: "https://m.media-amazon.com/images/M/MV5BNmIzNGI4ZDMtMzY3ZC00OGVmLTk5YTEtOGJlNTNlNmI0MTA4XkEyXkFqcGdeQXVyNTM3MDMyMDQ@._V1_UY1200_CR485,0,630,1200_AL_.jpg");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: "https://m.media-amazon.com/images/M/MV5BYzAxNzE5NTEtYjAwNS00ZWY3LTg1MWMtMjZlOTVlYTYxYzEzXkEyXkFqcGdeQXVyNDg2MjUxNjM@._V1_SY1000_CR0,0,1497,1000_AL_.jpg");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "FullName", "ImageURL", "NameAlias" },
                values: new object[] { "Movie Test Character", "", "Baloneer" });
        }
    }
}
