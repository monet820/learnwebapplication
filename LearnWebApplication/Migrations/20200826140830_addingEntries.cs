﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LearnWebApplication.Migrations
{
    public partial class addingEntries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Gender", "ImageURL", "NameAlias" },
                values: new object[,]
                {
                    { 2, "Heidi Bergman", "Female", "https://m.media-amazon.com/images/M/MV5BNmIzNGI4ZDMtMzY3ZC00OGVmLTk5YTEtOGJlNTNlNmI0MTA4XkEyXkFqcGdeQXVyNTM3MDMyMDQ@._V1_UY1200_CR485,0,630,1200_AL_.jpg", "Bergman" },
                    { 3, "Vuk", "Female", "https://m.media-amazon.com/images/M/MV5BYzAxNzE5NTEtYjAwNS00ZWY3LTg1MWMtMjZlOTVlYTYxYzEzXkEyXkFqcGdeQXVyNDg2MjUxNjM@._V1_SY1000_CR0,0,1497,1000_AL_.jpg", null }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 2, null, "Homecoming" },
                    { 3, null, "X-Men" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseID", "Genre", "ImageURL", "ReleaseDate", "Title", "Trailer" },
                values: new object[] { 3, "Sam Esmail", 2, "Drama", null, 2018, "Stop", null });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseID", "Genre", "ImageURL", "ReleaseDate", "Title", "Trailer" },
                values: new object[] { 2, "Simon Kinberg", 3, "Action", null, 2019, "Dark Phoenix", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
