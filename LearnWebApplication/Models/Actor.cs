﻿using System.Collections.Generic;


namespace LearnWebApplication.Models
{
    // Class to hold an actor. Actor is someone who is playing a character in an movie.
    public class Actor
    {
        // Primary key, ID
        public int Id { get; set; }

        // Foreign Key
        public ICollection<MovieCharacter> MovieCharacter { get; set; }

        // Field properties.
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }

        // Information of someone's life written by someone else.
        public string Biography { get; set; }
        // URL to an Image of the Actor.
        public string ImageURL { get; set; }

    }
}
