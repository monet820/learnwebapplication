﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace LearnWebApplication.Models
{
    // Character, someone who plays a role in an movie.
    public class Character
    {
        // Primary Key
        public int Id { get; set; }

        // Foreign Key and Relations
        public IList<MovieCharacter> MovieCharacter { get; set; }

        // Field values.
        public string FullName { get; set; }
        // Another name of the character.
        public string NameAlias { get; set; }
        public string Gender { get; set; }
        // URL to an Image of the character, or an idea of how he's tought to be.
        public string ImageURL { get; set; }

    }
}
