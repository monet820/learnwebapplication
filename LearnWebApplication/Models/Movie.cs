﻿using System.Collections.Generic;


namespace LearnWebApplication.Models
{
    // Class to hold a Movie
    public class Movie
    {
        // Primary Key
        public int Id { get; set; }
        // Foreign Key
        public IList<MovieCharacter> MovieCharacter { get; set; }

        public int FranchiseID { get; set; }
        public virtual Franchise Franchise { get; set; }

        // Field Values
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseDate { get; set; }
        public string Director { get; set; }
        public string ImageURL { get; set; }
        public string Trailer { get; set; }
    }
}
