﻿using Microsoft.EntityFrameworkCore;

namespace LearnWebApplication.Models
{
    public class MovieContext : DbContext
    {
        // Field Values.
        public Movie Movie { get; set; }
        public Franchise Franchise { get; set; }
        public Character Character { get; set; }
        public Actor Actor { get; set; }
        public MovieCharacter MovieCharacter { get; set; }

        // instantiate tables.
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MovieContext(DbContextOptions<MovieContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data for populating the database on creation.

            modelBuilder.Entity<Franchise>().HasData(new Franchise { Name = "Sisters", Id = 1 });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Name = "Homecoming", Id = 2 });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Name = "X-Men", Id = 3 });

            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Director = "James Atwater Contner", FranchiseID = 1, ReleaseDate = 1994, Genre = "Drama", Title = "I Only Have Eyes for You" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Director = "Simon Kinberg", FranchiseID = 3, ReleaseDate = 2019, Genre = "Action", Title = "Dark Phoenix" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Director = "Sam Esmail", FranchiseID = 2, ReleaseDate = 2018, Genre = "Drama", Title = "Stop" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, Director = "Robert mandel", FranchiseID = 1, ReleaseDate = 1994, Genre = "Drama", Title = "Blinders" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 5, Director = "Kevin inch", FranchiseID = 1, ReleaseDate = 1994, Genre = "Drama", Title = "Bombshell" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 6, Director = "Kevin inch", FranchiseID = 1, ReleaseDate = 1994, Genre = "Drama", Title = "Up in the air" });

            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName= "Detective James Falconer", Gender="Male", NameAlias = "Falconer"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName= "Heidi Bergman", Gender="Female", NameAlias = "Bergman"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName= "Vuk", Gender="Female"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName= "Hank McCoy", Gender="Male", NameAlias = "Beast"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 5, FullName= "Raven", Gender="Female", NameAlias = "Mystique"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 6, FullName= "Jean Grey", Gender="Female", NameAlias = "Phoenix"});

            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 1, FirstName = "Jessica", LastName = "Chastain", DateOfBirth = 1977, PlaceOfBirth = "Sacramento", Gender = "Female" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 2, FirstName = "Julia", LastName = "Roberts", DateOfBirth = 1967, PlaceOfBirth = "Smyrna", Gender = "Female" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 3, FirstName = "George", LastName = "Clooney", DateOfBirth = 1961, PlaceOfBirth = "Lexington", Gender = "Male" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 4, FirstName = "Sophie", LastName = "Turner", DateOfBirth = 1996, PlaceOfBirth = "Northampton", Gender = "Female" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 5, FirstName = "Jennifer", LastName = "Lawrence", DateOfBirth = 1990, PlaceOfBirth = "Kentucky", Gender = "Female" });
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 6, FirstName = "Nicholas", LastName = "Hoult", DateOfBirth = 1989, PlaceOfBirth = "Wokingham", Gender = "Male" });

            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 1, CharacterID = 3, MovieID = 2});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 2, CharacterID = 2, MovieID = 3});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 3, CharacterID = 1, MovieID = 1});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 4, CharacterID = 6, MovieID = 2});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 5, CharacterID = 5, MovieID = 2});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 6, CharacterID = 4, MovieID = 2});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 3, CharacterID = 1, MovieID = 4});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 3, CharacterID = 1, MovieID = 5});
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter{ ActorID = 3, CharacterID = 1, MovieID = 6});
            
            // Instantiate that the table MovieCharacter has two prviate keys.
            modelBuilder.Entity<MovieCharacter>().HasKey(pq => new { pq.MovieID, pq.CharacterID });
        }
    }
}
