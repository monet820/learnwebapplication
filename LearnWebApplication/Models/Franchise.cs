﻿using System.Collections.Generic;


namespace LearnWebApplication.Models
{
    // Franchise, a collection of similar movies / series.
       public class Franchise
    {
        // Primary Key
        public int Id { get; set; }

        // Foreign Key
        public ICollection<Movie> Movie { get; set; }

        // Field Values
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
