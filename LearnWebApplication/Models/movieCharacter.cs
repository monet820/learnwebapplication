﻿namespace LearnWebApplication.Models
{
    // Class to hold the ManyToMany between Character and movie.
    public class MovieCharacter
    {
        // Primary Key
        public int MovieID { get; set; }
        public virtual Movie Movie { get; set; }

        public int CharacterID { get; set; }
        public virtual Character Character { get; set; }
        // Foreign Key
        public int ActorID { get; set; }
        public virtual Actor Actor { get; set; }

        // Field Values.
        public string ImageURL { get; set; }
    }
}
