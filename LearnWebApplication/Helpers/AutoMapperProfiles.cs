﻿using AutoMapper;
using LearnWebApplication.Dtos;
using LearnWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LearnWebApplication.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        // Class to hold configurations of automapper.
        public AutoMapperProfiles()
        {
            // Mappings from Character to ->
            CreateMap<Character, ActorCharactersDto>();
            CreateMap<Character, CharacterDto>();

            // Mappings from Franchise to ->
            CreateMap<Franchise, FranchiseDto>();

            // Mappings from Actor to ->
            CreateMap<Actor, ActorDto>();
            CreateMap<Actor, CharacterActorsDto>();

            // Mappings from Movie to ->
            CreateMap<Movie, ActorMoviesDto>();
            CreateMap<Movie, MovieDto>();
            CreateMap<Movie, FranchiseMoviesDto>();

            // Mappings from MovieCharacter to ->
            CreateMap<MovieCharacter, MovieCharactersActorsDto>();
            CreateMap<MovieCharacter, MovieCharacterDto>();
        }
    }
}