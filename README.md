The project will create a .Net Core API project with database as code. The project focuses on relationships between an Actor, movies, franchises, characters they have played in and stored for use thorugh an API.

Running the project will open up swagger documentation. The Postman collection need to be installed, follow information below to get the tests. 
The project were created with the intent of learning about MVC, API, Swagger and Postman in .Net Core

How to install:
Clone to a local directory.
Open solution in Visual Studio
Run

To get postman collection, please click the button.
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/531861654dc4b3d92679)